package com.moza.thuctapmoza.modelmanager;

import android.content.Context;

import com.android.volley.VolleyError;
import com.moza.thuctapmoza.config.WebserviceConfig;
import com.moza.thuctapmoza.network.HttpError;
import com.moza.thuctapmoza.network.HttpGet;
import com.moza.thuctapmoza.network.HttpListener;
import com.moza.thuctapmoza.network.ParameterFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 28/06/2017.
 */

public class ModelManager {

    public ModelManager() {
    }

    public static void getAllPhoto(Context context, int page, int id, int type,
                                   boolean isShowDialog, final ModelManagerListener listener) {
        final String url = WebserviceConfig.getAllPhoto(context);

        Map<String, String> params = ParameterFactory.createPhotoParams(id, type, page);

        new HttpGet(context, url, params, isShowDialog, new HttpListener() {
            @Override
            public void onHttpRespones(Object response) {
                if (response != null) {
                    listener.onSuccess(response.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }

    public static void getRss(Context context, String url, boolean isShowDialog, final ModelManagerListener listener) {
        Map<String, String> params = new HashMap<>();
        new HttpGet(context, url, params, isShowDialog, new HttpListener() {
            @Override
            public void onHttpRespones(Object respones) {
                if (respones != null) {
                    listener.onSuccess(respones.toString());
                } else {
                    listener.onError(null);
                }
            }
        }, new HttpError() {
            @Override
            public void onHttpError(VolleyError volleyError) {
                listener.onError(volleyError);
            }
        });
    }
}
