package com.moza.thuctapmoza.modelmanager;

import android.util.Log;

import com.moza.thuctapmoza.model.XmlModel;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Admin on 17/7/2017.
 */

public class RssParseUtility {
    private String mXml;
    public static final String ITEM = "item";
    public static final String LINK = "link";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String PUBDATE = "pubDate";
    public static final String GUID = "guid";
    public static final String CHANNEL = "channel";

    public RssParseUtility(String mXml) {
        this.mXml = mXml;
    }

    public Document getDocumentFromXml(String xml) {
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setCoalescing(true);
        try {

            DocumentBuilder db = dbf.newDocumentBuilder();

            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));
            doc = db.parse(is);

        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        }
        // return DOM
        return doc;
    }

    public String getValue(Element item, String str) {
        NodeList n = item.getElementsByTagName(str);
        return this.getElementValue(n.item(0));
    }

    public final String getElementValue(Node element) {
        Node child;
        if (element != null) {
            if (element.hasChildNodes()) {
                for (child = element.getFirstChild(); child != null; child = child.getNextSibling()) {
                    if (child.getNodeType() == Node.TEXT_NODE || child.getNodeType() == Node.CDATA_SECTION_NODE) {
                        return child.getNodeValue().trim();
                    }
                }
            }
        }
        return "";
    }

    public ArrayList<XmlModel> getDataList() {
        ArrayList<XmlModel> lstModel = new ArrayList<>();
        Document doc = getDocumentFromXml(mXml);

        NodeList nodeList = doc.getElementsByTagName(ITEM);

        for (int i = 0; i < nodeList.getLength(); i++) {
            XmlModel model = new XmlModel();
            Element e = (Element) nodeList.item(i);

            model.setTitle(getValue(e, TITLE));
            model.setDescription(getValue(e, DESCRIPTION));
            model.setLink(getValue(e, LINK));
            model.setPubDate(getValue(e, PUBDATE));
            model.setGuid(getValue(e, GUID));

            lstModel.add(model);
        }

        return lstModel;
    }

}
