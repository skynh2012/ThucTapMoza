package com.moza.thuctapmoza.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.StaggOne;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Administrator on 22/06/2017.
 */

public class StaggOneAdapter extends RecyclerView.Adapter<StaggOneAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<StaggOne> mList;

    public StaggOneAdapter(Context mContext, ArrayList<StaggOne> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stagg_one, parent, false);
        ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        StaggOne staggOne = mList.get(position);

        if (staggOne != null) {
            if (!staggOne.getPhoto().equals("")) {
                Picasso.with(mContext)
                        .load(staggOne.getPhoto())
                        .into(holder.imvPhoto);
            }
            holder.tvTitle.setText(staggOne.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imvPhoto;
        public TextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            imvPhoto = (ImageView) itemView.findViewById(R.id.imv_stagg_one_photo);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_stagg_one_title);
        }
    }
}
