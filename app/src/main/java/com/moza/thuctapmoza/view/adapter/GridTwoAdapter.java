package com.moza.thuctapmoza.view.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.GridTwo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 21/06/2017.
 */

public class GridTwoAdapter extends RecyclerView.Adapter<GridTwoAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<GridTwo> mList;

    public GridTwoAdapter(Context mContext, ArrayList<GridTwo> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_two, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GridTwo two = mList.get(position);

        if (two != null) {
            if (!two.getCover().equals("")) {
                Picasso.with(mContext)
                        .load(two.getCover())
                        .into(holder.imvCover);
            }

            if (!two.getAvatar().equals("")) {
                Picasso.with(mContext)
                        .load(two.getAvatar())
                        .into(holder.imvAvatar);
            }
            if (two.isSex()) {
                holder.imvSex.setImageResource(R.drawable.ic_male);
            } else {
                holder.imvSex.setImageResource(R.drawable.icon_female);
            }

            holder.tvName.setText(two.getName());
            holder.tvAge.setText(String.valueOf(two.getAge()));
        }
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imvCover, imvSex;
        public CircleImageView imvAvatar;
        public TextView tvName, tvAge;

        public ViewHolder(View itemView) {
            super(itemView);
            imvCover = (ImageView) itemView.findViewById(R.id.imv_grid_two_cover);
            imvSex = (ImageView) itemView.findViewById(R.id.imv_grid_two_sex);
            imvAvatar = (CircleImageView) itemView.findViewById(R.id.imv_grid_two_avatar);
            tvName = (TextView) itemView.findViewById(R.id.tv_grid_two_name);
            tvAge = (TextView) itemView.findViewById(R.id.tv_grid_two_age);
        }
    }
}
