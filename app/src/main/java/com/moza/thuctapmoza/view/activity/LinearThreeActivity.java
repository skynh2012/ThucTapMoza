package com.moza.thuctapmoza.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.LinearThree;
import com.moza.thuctapmoza.view.adapter.LinearThreeAdapter;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class LinearThreeActivity extends AppCompatActivity {
    private RecyclerView rcvThree;
    private ArrayList<LinearThree> mListThree;
    private LinearThreeAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_three);

        rcvThree = (RecyclerView) findViewById(R.id.rcv_linear_three);
        mListThree = new ArrayList<>();
        mAdapter = new LinearThreeAdapter(LinearThreeActivity.this, mListThree);
        rcvThree.setLayoutManager(new LinearLayoutManager(LinearThreeActivity.this, LinearLayoutManager.VERTICAL, false));
        rcvThree.setAdapter(mAdapter);
        faceData();
    }

    private void faceData() {
        LinearThree three;

        three = new LinearThree();
        three.setRate(4.5f);
        three.setName("Newton King");
        three.setDate(DateFormat.getDateInstance().format(new Date()));
        three.setTitle("Use the standard Java DateFormat class");
        three.setContent("You can initialise a Date object with your own values, however you should be aware that the constructors have been deprecated and you should really be using a Java Calendar object.");
        mListThree.add(three);

        three = new LinearThree();
        three.setRate(4.5f);
        three.setName("Newton King");
        three.setDate(DateFormat.getDateInstance().format(new Date()));
        three.setTitle("Use the standard Java DateFormat class");
        three.setContent("You can initialise a Date object with your own values, however you should be aware that the constructors have been deprecated and you should really be using a Java Calendar object.");
        mListThree.add(three);

        three = new LinearThree();
        three.setRate(4.5f);
        three.setName("Newton King");
        three.setDate(DateFormat.getDateInstance().format(new Date()));
        three.setTitle("Use the standard Java DateFormat class");
        three.setContent("You can initialise a Date object with your own values, however you should be aware that the constructors have been deprecated and you should really be using a Java Calendar object.");
        mListThree.add(three);

        three = new LinearThree();
        three.setRate(4.5f);
        three.setName("Newton King");
        three.setDate(DateFormat.getDateInstance().format(new Date()));
        three.setTitle("Use the standard Java DateFormat class");
        three.setContent("You can initialise a Date object with your own values, however you should be aware that the constructors have been deprecated and you should really be using a Java Calendar object.");
        mListThree.add(three);

        mAdapter.notifyDataSetChanged();
    }
}
