package com.moza.thuctapmoza.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.config.Common;
import com.moza.thuctapmoza.model.GridOther;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Administrator on 22/06/2017.
 */

public class GridOtherAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_ONE = 0;
    private static final int TYPE_TWO = 1;
    private Context mContext;
    private ArrayList<GridOther> mList;

    public GridOtherAdapter(Context mContext, ArrayList<GridOther> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case TYPE_ONE:
                return new ViewHolderOne(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_other_type_one, parent, false));

            case TYPE_TWO:
                return new ViewHolderTwo(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_other_type_two, parent, false));

            default:
                return null;

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        GridOther otherOne = mList.get(position);

        if (otherOne != null) {
            if (holder.getItemViewType() == TYPE_ONE) {
                if (!otherOne.getPhoto().equals("")) {
                    Picasso.with(mContext)
                            .load(otherOne.getPhoto())
                            .into(((ViewHolderOne) holder).imvPhoto);
                }
                ((ViewHolderOne) holder).btnTopic.setText(otherOne.getTopic());
                switch (otherOne.getTopicColor()) {
                    case 0:
                        ((ViewHolderOne) holder).btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_blue));
                        break;

                    case 1:
                        ((ViewHolderOne) holder).btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_yellow));
                        break;

                    case 2:
                        ((ViewHolderOne) holder).btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_green));
                        break;

                    case 3:
                        ((ViewHolderOne) holder).btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_green_yellow));
                        break;

                    case 4:
                        ((ViewHolderOne) holder).btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_red));
                        break;

                    case 5:
                        ((ViewHolderOne) holder).btnTopic.setBackground(mContext.getDrawable(R.drawable.bg_radius_blue_violet));
                        break;
                }

            } else if (holder.getItemViewType() == TYPE_TWO) {
                ((ViewHolderTwo) holder).tvTime.setText(otherOne.getTime());
                ((ViewHolderTwo) holder).tvTitle.setText(otherOne.getTitle());
            }
        }
    }

    @Override
    public int getItemCount() {

        return mList != null ? mList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getType();
    }

    public class ViewHolderOne extends RecyclerView.ViewHolder {
        public ImageView imvPhoto;
        public TextView btnTopic;

        public ViewHolderOne(View itemView) {
            super(itemView);
            imvPhoto = (ImageView) itemView.findViewById(R.id.imv_grid_other_photo);
            btnTopic = (TextView) itemView.findViewById(R.id.btn_grid_other_topic);
        }
    }

    public class ViewHolderTwo extends RecyclerView.ViewHolder {
        public TextView tvTime, tvTitle;

        public ViewHolderTwo(View itemView) {
            super(itemView);
            tvTime = (TextView) itemView.findViewById(R.id.tv_grid_other_time);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_grid_other_title);

        }
    }
}
