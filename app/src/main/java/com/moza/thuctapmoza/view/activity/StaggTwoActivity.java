package com.moza.thuctapmoza.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.MenuItem;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.StaggTwo;
import com.moza.thuctapmoza.util.ActionBarUtil;
import com.moza.thuctapmoza.view.adapter.StaggTwoAdapter;

import java.util.ArrayList;

public class StaggTwoActivity extends AppCompatActivity {
    private RecyclerView rcvStaggTwo;
    private ArrayList<StaggTwo> mArrTwo;
    private StaggTwoAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stagg_two);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBarUtil.setTitleCenterActionBar("Popular", getSupportActionBar());

        rcvStaggTwo = (RecyclerView) findViewById(R.id.rcv_stagg_two);

        mArrTwo = new ArrayList<>();
        mAdapter = new StaggTwoAdapter(StaggTwoActivity.this, mArrTwo);

        rcvStaggTwo.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        rcvStaggTwo.setAdapter(mAdapter);
        faceData();
    }

    private void faceData() {
        StaggTwo two;

        two = new StaggTwo();
        two.setPhoto("http://diendanamthuc.com.vn/upload/suc-khoe-va-dinh-duong/lam-dep/trai-cay-tri-mun/trai-cay-tri-mun-1.jpg");
        two.setName("Cam quýt");
        two.setTitle("Các loại trái cây trị mụn hiệu quả");
        two.setLike(123);
        two.setLiked(false);
        two.setTime("10 minutes ago");
        two.setTopic("Fruit");
        two.setTopicColor(0);

        mArrTwo.add(two);

        two = new StaggTwo();
        two.setPhoto("http://www.seriouseats.com/assets_c/2015/02/20150206-tea-vicky-wasik-black-tea-service-thumb-1500xauto-419131.jpg");
        two.setName("Tra");
        two.setTitle("Các loại trà trị mụn hiệu quả");
        two.setLike(125);
        two.setLiked(true);
        two.setTime("11 minutes ago");
        two.setTopic("Animal");
        two.setTopicColor(1);

        mArrTwo.add(two);

        two = new StaggTwo();
        two.setPhoto("https://tutudao.files.wordpress.com/2013/03/200862131643574_2.jpg");
        two.setName("Giỏ hoa quả");
        two.setTitle("Vào những ngày đặc biệt như sinh nhật Bố Mẹ, Sinh nhật Thầy cô, hay tặng quà mừng khai trương văn phòng nhỏ");
        two.setLike(145);
        two.setLiked(true);
        two.setTime("10 minutes ago");
        two.setTopic("Fruit");
        two.setTopicColor(2);

        mArrTwo.add(two);

        two = new StaggTwo();
        two.setPhoto("http://www.wesleyacademy.org/wesleyadmin/wp-content/uploads/2012/08/technology1.jpg");
        two.setName("Cam quýt");
        two.setTitle("Các loại trái cây trị mụn hiệu quả");
        two.setLike(123);
        two.setLiked(false);
        two.setTime("10 minutes ago");
        two.setTopic("Technology");
        two.setTopicColor(3);

        mArrTwo.add(two);

        two = new StaggTwo();
        two.setPhoto("http://www.thekingsleyinn.com/d/thekingsleyinn/media/Amenities/__thumbs_1600_1024_crop/Film.jpg");
        two.setName("film");
        two.setTitle("Các loại film ị mụn hiệu quả");
        two.setLike(123);
        two.setLiked(false);
        two.setTime("10 minutes ago");
        two.setTopic("Film");
        two.setTopicColor(4);

        mArrTwo.add(two);

        two = new StaggTwo();
        two.setPhoto("http://diendanamthuc.com.vn/upload/suc-khoe-va-dinh-duong/lam-dep/trai-cay-tri-mun/trai-cay-tri-mun-1.jpg");
        two.setName("Cam quýt");
        two.setTitle("Các loại trái cây trị mụn hiệu quả");
        two.setLike(123);
        two.setLiked(false);
        two.setTime("10 minutes ago");
        two.setTopic("Animal");
        two.setTopicColor(5);

        mArrTwo.add(two);

        two = new StaggTwo();
        two.setPhoto("http://diendanamthuc.com.vn/upload/suc-khoe-va-dinh-duong/lam-dep/trai-cay-tri-mun/trai-cay-tri-mun-1.jpg");
        two.setName("Cam quýt");
        two.setTitle("Các loại trái cây trị mụn hiệu quả");
        two.setLike(123);
        two.setLiked(false);
        two.setTime("10 minutes ago");
        two.setTopic("Animal");
        two.setTopicColor(6);

        mArrTwo.add(two);

        two = new StaggTwo();
        two.setPhoto("http://diendanamthuc.com.vn/upload/suc-khoe-va-dinh-duong/lam-dep/trai-cay-tri-mun/trai-cay-tri-mun-1.jpg");
        two.setName("Cam quýt");
        two.setTitle("Các loại trái cây trị mụn hiệu quả");
        two.setLike(123);
        two.setLiked(false);
        two.setTime("10 minutes ago");
        two.setTopic("Animal");
        two.setTopicColor(0);

        mArrTwo.add(two);

        two = new StaggTwo();
        two.setPhoto("http://diendanamthuc.com.vn/upload/suc-khoe-va-dinh-duong/lam-dep/trai-cay-tri-mun/trai-cay-tri-mun-1.jpg");
        two.setName("Cam quýt");
        two.setTitle("Các loại trái cây trị mụn hiệu quả");
        two.setLike(123);
        two.setLiked(false);
        two.setTime("10 minutes ago");
        two.setTopic("Animal");
        two.setTopicColor(0);

        mArrTwo.add(two);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
