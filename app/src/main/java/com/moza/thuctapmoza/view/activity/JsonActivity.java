package com.moza.thuctapmoza.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.GridTwo;
import com.moza.thuctapmoza.model.Photo;
import com.moza.thuctapmoza.networkgson.ApiResponse;
import com.moza.thuctapmoza.view.adapter.GridTwoAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class JsonActivity extends AppCompatActivity {
    RecyclerView rcvJson;
    GridTwoAdapter mAdapter;
    ArrayList<GridTwo> mArrTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json);

        initView();
        loadData();
    }

    private void initView() {

        rcvJson = (RecyclerView) findViewById(R.id.rcv_read_json);
        mArrTwo = new ArrayList<>();
        mAdapter = new GridTwoAdapter(JsonActivity.this, mArrTwo);
        rcvJson.setLayoutManager(new GridLayoutManager(JsonActivity.this, 2, LinearLayoutManager.VERTICAL, false));
        rcvJson.setAdapter(mAdapter);
    }

    private String loadJsonFromAssets() {
        String json = null;

        try {
            InputStream stream = this.getAssets().open("jsontest.json");
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return json;
    }

    private void loadData() {
        String jsonData = loadJsonFromAssets();
        Toast.makeText(JsonActivity.this, "Content in file jsontest.json is : " + jsonData, Toast.LENGTH_LONG).show();

        try {
            JSONObject obj = new JSONObject(jsonData);

            ApiResponse response = new ApiResponse(obj);

            List<Photo> lstPhoto = response.getDataList(Photo.class);

            for (Photo photo : lstPhoto) {
                GridTwo two = new GridTwo();
                two.setCover(photo.getImages());
                two.setName(photo.getName());
                two.setSex(false);
                two.setAvatar(photo.getImages());
                two.setAge(photo.getView());

                mArrTwo.add(two);
            }
            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
