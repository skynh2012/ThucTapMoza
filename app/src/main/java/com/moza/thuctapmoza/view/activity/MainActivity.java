package com.moza.thuctapmoza.view.activity;
/**
 * mainactivity.java
 *
 * @author duyanh
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.base.RecycleViewOnTouchListener;
import com.moza.thuctapmoza.config.Common;
import com.moza.thuctapmoza.model.MainList;
import com.moza.thuctapmoza.view.adapter.MainRecycleAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rcvMain;
    private ArrayList<MainList> mList;
    private MainRecycleAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rcvMain = (RecyclerView) findViewById(R.id.rcv_main);

        mList = new ArrayList<>();
        mAdapter = new MainRecycleAdapter(this, mList);
        rcvMain.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        rcvMain.setAdapter(mAdapter);
        faceData();

        rcvMain.addOnItemTouchListener(new RecycleViewOnTouchListener(this, new RecycleViewOnTouchListener.OnItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                switch (position) {
                    case 0:
                        startActivity(new Intent(MainActivity.this, RecycleTestActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(MainActivity.this, NetworkActivity.class));
                        break;

                    case 2:
                        startActivity(new Intent(MainActivity.this, FragmentTestActivity.class));
                        break;
                    case 3:
                        startActivity(new Intent(MainActivity.this, SqliteTestActivity.class));
                        break;

                    case 4:
                        startActivity(new Intent(MainActivity.this, ReadJsonXmlRssActivity.class));
                        break;


                }
            }
        }));
    }

    private void faceData() {
        MainList list;

        list = new MainList(Common.URL_AVATAR_1, getResources().getString(R.string.main_title_1));
        mList.add(list);

        list = new MainList(Common.URL_AVATAR_4, getResources().getString(R.string.main_title_4));
        mList.add(list);

        list = new MainList(Common.URL_AVATAR_2, getResources().getString(R.string.main_title_2));
        mList.add(list);

        list = new MainList(Common.URL_AVATAR_3, getResources().getString(R.string.main_title_3));
        mList.add(list);

        list = new MainList(Common.URL_AVATAR_3, getResources().getString(R.string.main_title_5));
        mList.add(list);

        mAdapter.notifyDataSetChanged();
    }
}
