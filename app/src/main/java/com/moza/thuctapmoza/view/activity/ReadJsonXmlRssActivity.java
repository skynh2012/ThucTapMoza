package com.moza.thuctapmoza.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Xml;
import android.view.View;
import android.widget.Button;

import com.moza.thuctapmoza.R;

public class ReadJsonXmlRssActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnReadJson, btnReadXml, btnReadRss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_json_xml_rss);
        initView();
    }

    private void initView() {
        btnReadJson = (Button) findViewById(R.id.btn_read_json);
        btnReadXml = (Button) findViewById(R.id.btn_read_xml);
        btnReadRss = (Button) findViewById(R.id.btn_read_rss);

        btnReadJson.setOnClickListener(this);
        btnReadXml.setOnClickListener(this);
        btnReadRss.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.btn_read_json) {
            startActivity(new Intent(ReadJsonXmlRssActivity.this, JsonActivity.class));
        } else if (id == R.id.btn_read_xml) {
            startActivity(new Intent(ReadJsonXmlRssActivity.this, XmlActivity.class));
        } else if (id == R.id.btn_read_rss) {
            startActivity(new Intent(ReadJsonXmlRssActivity.this, RssActivity.class));
        }
    }
}
