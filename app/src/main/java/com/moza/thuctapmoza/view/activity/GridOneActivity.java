package com.moza.thuctapmoza.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.GridOne;
import com.moza.thuctapmoza.util.ActionBarUtil;
import com.moza.thuctapmoza.view.adapter.GridOneAdapter;

import java.util.ArrayList;

public class GridOneActivity extends AppCompatActivity {
    private RecyclerView rcvGridOne;
    private ArrayList<GridOne> mListOne;
    private GridOneAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_one);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBarUtil.setTitleCenterActionBar("Home", getSupportActionBar());
        rcvGridOne = (RecyclerView) findViewById(R.id.rcv_grid_one);

        mListOne = new ArrayList<>();

        mAdapter = new GridOneAdapter(GridOneActivity.this, mListOne);

        rcvGridOne.setLayoutManager(new GridLayoutManager(GridOneActivity.this, 2, LinearLayoutManager.VERTICAL, false));
        rcvGridOne.setAdapter(mAdapter);
        faceData();
    }

    private void faceData() {
        GridOne one;

        one = new GridOne();
        one.setCover("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/12662530_949180708485116_5507960975209644340_n.jpg?oh=eb8bf637c2792f9e91c6bc778c0bd025&oe=59CB04F1");
        one.setAvatar("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18485870_1363090603760789_2641997873857935406_n.jpg?oh=5ac6c2795dc5c1e3ddd4067c6362c534&oe=59D569FE");
        one.setTitle("This formatter only includes the date, not the time as the original question stated");
        one.setLike(2032);
        one.setComment(928);
        mListOne.add(one);

        one = new GridOne();
        one.setCover("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/10375114_461240007355590_815042975923160520_n.jpg?oh=a421cd5b2b6b58db17b5d978a74c5cb2&oe=59C9C129");
        one.setAvatar("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/381335_135412693271658_1591982312_n.jpg?oh=1a6d54d23793c36d5bf93c9cdcb62600&oe=59D1E37C");
        one.setTitle("This formatter only includes the date, not the time as the original question stated");
        one.setLike(2031);
        one.setComment(928);
        mListOne.add(one);

        one = new GridOne();
        one.setCover("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18893332_1870761219844723_7824975202646215075_n.jpg?oh=486eea00fb37c1d49e7acd94362401fe&oe=59C7F8AF");
        one.setAvatar("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-1/18423916_1860044110916434_1680931345939881006_n.jpg?oh=4ce15a29af0243194e979077e41c4ce5&oe=59C93AF4");
        one.setTitle("This formatter only includes the date, not the time as the original question stated");
        one.setLike(2031);
        one.setComment(928);
        mListOne.add(one);

        one = new GridOne();
        one.setCover("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18424090_823564807798619_161235216898392296_n.jpg?oh=b5580a935ad2a99fd80f69c886405af7&oe=59D17C50");
        one.setAvatar("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18920502_835188009969632_4587356769002424254_n.jpg?oh=b8359914e0741effaa59c20cdfb10d0c&oe=59DC4CE1");
        one.setTitle("This formatter only includes the date, not the time as the original question stated");
        one.setLike(2031);
        one.setComment(928);
        mListOne.add(one);

        one = new GridOne();
        one.setCover("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/19059788_105072663435449_5475522350242311820_n.jpg?oh=0e67042429107274778d5716607712d7&oe=59D82AEE");
        one.setAvatar("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18814401_106818266581313_7075952417624994396_n.jpg?oh=e1669d2b1769a3098746b1d194f61247&oe=59E769AA");
        one.setTitle("This formatter only includes the date, not the time as the original question stated");
        one.setLike(2031);
        one.setComment(928);
        mListOne.add(one);

        one = new GridOne();
        one.setCover("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/12662530_949180708485116_5507960975209644340_n.jpg?oh=eb8bf637c2792f9e91c6bc778c0bd025&oe=59CB04F1");
        one.setAvatar("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18485870_1363090603760789_2641997873857935406_n.jpg?oh=5ac6c2795dc5c1e3ddd4067c6362c534&oe=59D569FE");
        one.setTitle("This formatter only includes the date, not the time as the original question stated");
        one.setLike(2031);
        one.setComment(928);
        mListOne.add(one);

        one = new GridOne();
        one.setCover("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18698518_104345253494594_2060239173917078300_n.jpg?oh=df9535c1cbf393794bf240de60fd1214&oe=59C90C3B");
        one.setAvatar("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18893280_112457302683389_1658519265453986203_n.jpg?oh=8f03fcb7156dff0fc41c3239229bb120&oe=59E453AC");
        one.setTitle("This formatter only includes the date, not the time as the original question stated");
        one.setLike(2031);
        one.setComment(928);
        mListOne.add(one);

        one = new GridOne();
        one.setCover("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/12662530_949180708485116_5507960975209644340_n.jpg?oh=eb8bf637c2792f9e91c6bc778c0bd025&oe=59CB04F1");
        one.setAvatar("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18485870_1363090603760789_2641997873857935406_n.jpg?oh=5ac6c2795dc5c1e3ddd4067c6362c534&oe=59D569FE");
        one.setTitle("This formatter only includes the date, not the time as the original question stated");
        one.setLike(2031);
        one.setComment(928);
        mListOne.add(one);

        one = new GridOne();
        one.setCover("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/12662530_949180708485116_5507960975209644340_n.jpg?oh=eb8bf637c2792f9e91c6bc778c0bd025&oe=59CB04F1");
        one.setAvatar("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18485870_1363090603760789_2641997873857935406_n.jpg?oh=5ac6c2795dc5c1e3ddd4067c6362c534&oe=59D569FE");
        one.setTitle("This formatter only includes the date, not the time as the original question stated");
        one.setLike(2031);
        one.setComment(928);
        mListOne.add(one);

        one = new GridOne();
        one.setCover("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/12662530_949180708485116_5507960975209644340_n.jpg?oh=eb8bf637c2792f9e91c6bc778c0bd025&oe=59CB04F1");
        one.setAvatar("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18485870_1363090603760789_2641997873857935406_n.jpg?oh=5ac6c2795dc5c1e3ddd4067c6362c534&oe=59D569FE");
        one.setTitle("This formatter only includes the date, not the time as the original question stated");
        one.setLike(2031);
        one.setComment(928);
        mListOne.add(one);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
