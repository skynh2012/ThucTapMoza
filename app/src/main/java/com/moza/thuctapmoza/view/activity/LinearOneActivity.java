package com.moza.thuctapmoza.view.activity;
/**
 * linearlayoutexampleoneactivity.java
 *
 * @author duyanh
 */

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.base.SimpleDividerItemDecoration;
import com.moza.thuctapmoza.model.LinearOne;
import com.moza.thuctapmoza.util.ActionBarUtil;
import com.moza.thuctapmoza.view.adapter.LinearOneAdapter;

import java.util.ArrayList;

/**
 *
 */
public class LinearOneActivity extends AppCompatActivity {
    private RecyclerView rcvLinearOne;
    private ArrayList<LinearOne> mList;
    private LinearOneAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_layout_example_one);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  // show back button
        ActionBarUtil.setTitleCenterActionBar(getResources().getString(R.string.title_actionbar_linear_one), getSupportActionBar()); // set title action bar
        rcvLinearOne = (RecyclerView) findViewById(R.id.rcv_linear_one); // find id recycle view

        mList = new ArrayList<>();
        mAdapter = new LinearOneAdapter(LinearOneActivity.this, mList);
        rcvLinearOne.setLayoutManager(new LinearLayoutManager(LinearOneActivity.this, LinearLayoutManager.VERTICAL, false));
        rcvLinearOne.addItemDecoration(new SimpleDividerItemDecoration(LinearOneActivity.this));
        rcvLinearOne.setAdapter(mAdapter);
        faceData();
    }

    private void faceData() {
        LinearOne linearOne;

        linearOne = new LinearOne("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18581885_102713216983784_532297111759719239_n.jpg?oh=467792e501029c9912ce129a19939a08&oe=59E5843A", "Đá phong thủy"
                , "10 mins", "Olympic của bọn nhóc còn căng thẳng hơn cả của người lớn nhé! Ai yếu tim thì đừng xem nhé kẻo tan chảy hết đó");
        mList.add(linearOne);

        linearOne = new LinearOne("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-1/18423916_1860044110916434_1680931345939881006_n.jpg?oh=4ce15a29af0243194e979077e41c4ce5&oe=59C93AF4", "Đoàn Hiển"
                , "20 mins", "Mới bán hàng thời gian đầu tất nhiên lượng khách ít, thì phải suy nghĩ làm cách nào bán được hàng");
        mList.add(linearOne);

        linearOne = new LinearOne("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18622334_107019853221215_4334463772149680186_n.jpg?oh=3a81ae1226c3b3c887d39782d3c9b195&oe=59DCEE02", "Văn Lê Ngọc"
                , "30 mins", "Thấy người ta bán hàng cả vài triệu mỗi ngày, trăm triệu mỗi tháng, khách sỉ cứ ào ào");
        mList.add(linearOne);

        linearOne = new LinearOne("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/19247578_235766453606057_2412066955061891129_n.jpg?oh=3407b7d6daa5954c904a411e87c6d623&oe=599E33FA", "Đinh Thảo"
                , "40 mins", "bán nhanh đang được giá, 62 tỷ nhớ giao dịch tại ngân hàng xong gửi tiền luôn kẻo bị cướp nhá");
        mList.add(linearOne);

        linearOne = new LinearOne("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/14991992_1109104635871548_3496679344670466378_n.jpg?oh=e33a61f65e6c55384f0fc9fdebe2f4c0&oe=59DDF347", "Trương Dũng"
                , "50 mins", "Olympic của bọn nhóc còn căng thẳng hơn cả của người lớn nhé! Ai yếu tim thì đừng xem nhé kẻo tan chảy hết đó");
        mList.add(linearOne);

        linearOne = new LinearOne("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18423939_240573569752856_2730153116499905337_n.jpg?oh=979d29d21fb8677db5a5a5f4843f8477&oe=59E97975", "Mun Trang"
                , "60 mins", "Olympic của bọn nhóc còn căng thẳng hơn cả của người lớn nhé! Ai yếu tim thì đừng xem nhé kẻo tan chảy hết đó");
        mList.add(linearOne);

        linearOne = new LinearOne("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18581885_102713216983784_532297111759719239_n.jpg?oh=467792e501029c9912ce129a19939a08&oe=59E5843A", "Đá phong thủy"
                , "1 hour 10 mins", "Olympic của bọn nhóc còn căng thẳng hơn cả của người lớn nhé! Ai yếu tim thì đừng xem nhé kẻo tan chảy hết đó");
        mList.add(linearOne);

        linearOne = new LinearOne("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18581885_102713216983784_532297111759719239_n.jpg?oh=467792e501029c9912ce129a19939a08&oe=59E5843A", "Đá phong thủy"
                , "1 hour 20 mins", "Olympic của bọn nhóc còn căng thẳng hơn cả của người lớn nhé! Ai yếu tim thì đừng xem nhé kẻo tan chảy hết đó");
        mList.add(linearOne);

        linearOne = new LinearOne("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18581885_102713216983784_532297111759719239_n.jpg?oh=467792e501029c9912ce129a19939a08&oe=59E5843A", "Đá phong thủy"
                , "1 hour 30 mins", "Olympic của bọn nhóc còn căng thẳng hơn cả của người lớn nhé! Ai yếu tim thì đừng xem nhé kẻo tan chảy hết đó");
        mList.add(linearOne);

        linearOne = new LinearOne("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18581885_102713216983784_532297111759719239_n.jpg?oh=467792e501029c9912ce129a19939a08&oe=59E5843A", "Đá phong thủy"
                , "1 hour 40 mins", "Olympic của bọn nhóc còn căng thẳng hơn cả của người lớn nhé! Ai yếu tim thì đừng xem nhé kẻo tan chảy hết đó");
        mList.add(linearOne);

        linearOne = new LinearOne("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18581885_102713216983784_532297111759719239_n.jpg?oh=467792e501029c9912ce129a19939a08&oe=59E5843A", "Đá phong thủy"
                , "1 hour 50 mins", "Olympic của bọn nhóc còn căng thẳng hơn cả của người lớn nhé! Ai yếu tim thì đừng xem nhé kẻo tan chảy hết đó");
        mList.add(linearOne);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
