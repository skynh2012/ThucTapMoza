package com.moza.thuctapmoza.view.activity;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.LinearTwo;
import com.moza.thuctapmoza.view.adapter.LinearTwoAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class LinearTwoActivity extends AppCompatActivity {
    private CircleImageView imvAvatar;
    private RecyclerView rcvLinearTwo;
    private ArrayList<LinearTwo> mListTwo;
    private LinearTwoAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_two);

        imvAvatar = (CircleImageView) findViewById(R.id.imv_avatar);
        Picasso.with(LinearTwoActivity.this)
                .load("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18581885_102713216983784_532297111759719239_n.jpg?oh=467792e501029c9912ce129a19939a08&oe=59E5843A")
                .into(imvAvatar);

        rcvLinearTwo = (RecyclerView) findViewById(R.id.rcv_linear_two);
        mListTwo = new ArrayList<>();
        mAdapter = new LinearTwoAdapter(LinearTwoActivity.this, mListTwo);
        rcvLinearTwo.setLayoutManager(new LinearLayoutManager(LinearTwoActivity.this, LinearLayoutManager.VERTICAL, false));
        rcvLinearTwo.setAdapter(mAdapter);
        faceData();
    }

    private void faceData() {
        LinearTwo two;

        two = new LinearTwo();
        two.setAvata("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/14937331_706538752855950_5619287457576822761_n.jpg?oh=8b561c57c6ae95c0ec41f3352b925646&oe=59D3EB72");
        two.setImage("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/19366198_848684591974698_3610454056748112955_n.jpg?oh=00e98bd3f4267b002a7c61b143067368&oe=59E61132");
        two.setName("Rockin' Cupcakes");
        two.setTime("10 mins");
        two.setMessage("Celebrating the beginning of summer… School is officially out! Best friends!!!");

        mListTwo.add(two);

        two = new LinearTwo();
        two.setAvata("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/13615324_641952679299089_248782922189501307_n.jpg?oh=fbbc0c378d5c93cabe39efddfdcd2565&oe=59D10863");
        two.setImage("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/19274940_827102920784063_8266232311968604922_n.png?oh=78e36d3e3f0b67c22301a469406e543e&oe=59D3CAB2");
        two.setName("Top comments");
        two.setTime("15 mins");
        two.setMessage("Quảng cáo nào làm các mày nhớ mãi?");

        mListTwo.add(two);

        two = new LinearTwo();
        two.setAvata("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/18920424_906574049491919_5169375161517177350_n.jpg?oh=2a04a4bb134ebe1d8f3c4f83839e7e19&oe=59E541E0");
        two.setImage("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/19225499_1968818483399622_420504326533967205_n.jpg?oh=57099d27212de1449d9141d34d664536&oe=59C9EE5A");
        two.setName("Susu Thuy Hai");
        two.setTime("18 mins");
        two.setMessage("Chúc chị hải sn vv nhé\uD83D\uDE18\uD83D\uDE18");

        mListTwo.add(two);

        two = new LinearTwo();
        two.setAvata("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/19145870_869427189875398_1242670165114615059_n.jpg?oh=9a9bebd4effddfe6a23ec9b7740321e9&oe=59D581E2");
        two.setImage("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/19225876_870816963069754_5627098024109261643_n.jpg?oh=b9caa659c8337f13c33253e5aed40534&oe=59E02887");
        two.setName("Dung Nhi");
        two.setTime("20 mins");
        two.setMessage("Ahihi");

        mListTwo.add(two);

        two = new LinearTwo();
        two.setAvata("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/13615324_641952679299089_248782922189501307_n.jpg?oh=fbbc0c378d5c93cabe39efddfdcd2565&oe=59D10863");
        two.setImage("https://scontent.fhan5-1.fna.fbcdn.net/v/t1.0-9/19274940_827102920784063_8266232311968604922_n.png?oh=78e36d3e3f0b67c22301a469406e543e&oe=59D3CAB2");
        two.setName("Top comments");
        two.setTime("10 mins");
        two.setMessage("Quảng cáo nào làm các mày nhớ mãi?");

        mListTwo.add(two);

        mAdapter.notifyDataSetChanged();
    }
}
