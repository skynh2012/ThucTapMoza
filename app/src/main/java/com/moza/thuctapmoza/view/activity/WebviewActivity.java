package com.moza.thuctapmoza.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.moza.thuctapmoza.R;

public class WebviewActivity extends AppCompatActivity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        initView();
    }

    private void initView() {
        webView = (WebView) findViewById(R.id.wv_test);

        String url = getIntent().getStringExtra("link");
        if (url != null) {
            webView.loadUrl(url);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setSupportZoom(true);
        }
        webView.setWebViewClient(new WebViewClient());
    }
}
