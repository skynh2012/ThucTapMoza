package com.moza.thuctapmoza.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.LinearOne;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Administrator on 20/06/2017.
 */

public class LinearOneAdapter extends RecyclerView.Adapter<LinearOneAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<LinearOne> mList;

    public LinearOneAdapter(Context mContext, ArrayList<LinearOne> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_linear_one, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LinearOne linearOne = mList.get(position);

        if (linearOne != null) {
            if (!linearOne.getAvatar().equals("")) {
                Picasso.with(mContext)
                        .load(linearOne.getAvatar())
                        .into(holder.imvLinearOneAvatar);
            }

            holder.tvLinearOneName.setText(linearOne.getName());
            holder.tvLinearOneTime.setText(linearOne.getTime());
            holder.tvLinearOneMessage.setText(linearOne.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imvLinearOneAvatar;
        public TextView tvLinearOneName, tvLinearOneTime, tvLinearOneMessage;

        public ViewHolder(View itemView) {
            super(itemView);
            imvLinearOneAvatar = (CircleImageView) itemView.findViewById(R.id.imv_linear_one_avata);
            tvLinearOneName = (TextView) itemView.findViewById(R.id.tv_linear_one_name);
            tvLinearOneTime = (TextView) itemView.findViewById(R.id.tv_linear_one_time);
            tvLinearOneMessage = (TextView) itemView.findViewById(R.id.tv_linear_one_message);
        }
    }
}
