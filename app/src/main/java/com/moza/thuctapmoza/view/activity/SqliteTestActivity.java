package com.moza.thuctapmoza.view.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.config.WebserviceConfig;
import com.moza.thuctapmoza.database.DatabaseUtility;
import com.moza.thuctapmoza.database.SQLiteManager;
import com.moza.thuctapmoza.model.PhotoInfo;
import com.moza.thuctapmoza.network.ParameterFactory;
import com.moza.thuctapmoza.networkgson.ApiManager;
import com.moza.thuctapmoza.networkgson.ApiResponse;
import com.moza.thuctapmoza.view.adapter.SqliteTestAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SqliteTestActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();
    ListView lsvSqltieTest;
    Button btnDelete, btnAdd, btnFilter;
    private ArrayList<PhotoInfo> mPhoto;
    private SqliteTestAdapter mAdapter;
    private SQLiteManager manager;
    private CheckBox btnSelectAll;
    private boolean isCheckAll = false;
    private final String url = "http://27.72.88.241:8888/hicom-photos/images/images/141040296414099063603.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sqlite_test);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Thực hành database");

        initView();

        if (manager == null) {
            manager = new SQLiteManager(SqliteTestActivity.this);
        }
        mPhoto = new ArrayList<>();

        mAdapter = new SqliteTestAdapter(SqliteTestActivity.this, R.layout.item_sqlite_test, mPhoto);

        lsvSqltieTest.setAdapter(mAdapter);
        loadPhoto();
        btnDelete.setOnClickListener(this);
        btnSelectAll.setOnClickListener(this);
        btnFilter.setOnClickListener(this);
        btnAdd.setOnClickListener(this);
        lsvSqltieTest.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long which) {
                PhotoInfo photoInfo = mPhoto.get(position);
                showDetails(photoInfo);
            }
        });

        ApiManager.get(WebserviceConfig.getAllPhoto(SqliteTestActivity.this), ParameterFactory.setPhotoParams(1, 0, 1), new ApiManager.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                List<PhotoInfo> lstPhoto = response.getDataList(PhotoInfo.class);
                /*if (manager == null) {
                    manager = new SQLiteManager(SqliteTestActivity.this);
                }*/
                for (PhotoInfo info : lstPhoto) {
                    DatabaseUtility.getInstance(SqliteTestActivity.this).insertPhoto(info);
                }
                loadPhoto();

            }

            @Override
            public void onError(String message) {
                Log.e(TAG, message);
            }
        });


    }

    private void initView() {
        lsvSqltieTest = (ListView) findViewById(R.id.lsv_sqlite_test);
        btnDelete = (Button) findViewById(R.id.btn_sqlite_test_delete);
        btnSelectAll = (CheckBox) findViewById(R.id.btn_sqlite_test_select_all);
        btnAdd = (Button) findViewById(R.id.btn_sqlite_test_add);
        btnFilter = (Button) findViewById(R.id.btn_sqlite_test_filter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.btn_sqlite_test_delete) {
            delete();
        } else if (id == R.id.btn_sqlite_test_select_all) {
            selectAll();
        } else if (id == R.id.btn_sqlite_test_add) {
            showDetails(null);
        } else if (id == R.id.btn_sqlite_test_filter) {
            showPopup(view);
        }
    }

    private void selectAll() {
        isCheckAll = !isCheckAll;
        for (int i = 0; i < mPhoto.size(); i++) {
            mPhoto.get(i).setCheck(isCheckAll);
        }
        mAdapter.notifyDataSetChanged();
    }

    private void delete() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(SqliteTestActivity.this);
        mBuilder.setTitle("Xóa dữ liệu");
        mBuilder.setMessage("Bạn có chắc chắn muốn xóa");
        mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        mBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                for (int i = mPhoto.size() - 1; i >= 0; i--) {
                    if (mPhoto.get(i).isCheck()) {
                        DatabaseUtility.getInstance(SqliteTestActivity.this).deletePhoto(mPhoto.get(i));
                    }
                }
                mPhoto.clear();
                loadPhoto();
                dialogInterface.dismiss();
            }
        });

        if (getitemCheck()) {
            mBuilder.show();
        }
    }

    private boolean getitemCheck() {
        boolean isCheck = false;

        for (int i = 0; i < mPhoto.size(); i++) {
            if (mPhoto.get(i).isCheck()) {
                isCheck = true;
                break;
            }
        }
        return isCheck;
    }

    private void loadPhoto() {
       /* if (manager == null) {
            manager = new SQLiteManager(SqliteTestActivity.this);
        }
        ArrayList<PhotoInfo> lstPhoto = manager.getAllPhoto();*/

        List<PhotoInfo> lstPhoto = DatabaseUtility.getInstance(this).getListPhoto();

        for (PhotoInfo info : lstPhoto) {
            if (!mPhoto.contains(info)) {
                mPhoto.add(info);
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    private void showPopup(View view) {
        PopupMenu popupMenu = new PopupMenu(SqliteTestActivity.this, view, Gravity.CENTER);
        popupMenu.inflate(R.menu.menu_filter);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.mn_filter_asc) {
                    Collections.sort(mPhoto, new Comparator<PhotoInfo>() {
                        @Override
                        public int compare(PhotoInfo info, PhotoInfo t1) {
                            if (info.getId() > t1.getId()) {
                                return 1;
                            } else if (info.getId() == t1.getId()) {
                                return 0;
                            } else {
                                return -1;
                            }
                        }
                    });
                } else if (id == R.id.mn_filter_des) {
                    Collections.sort(mPhoto, new Comparator<PhotoInfo>() {
                        @Override
                        public int compare(PhotoInfo info, PhotoInfo t1) {
                            if (info.getId() > t1.getId()) {
                                return -1;
                            } else if (info.getId() == t1.getId()) {
                                return 0;
                            } else {
                                return 1;
                            }
                        }
                    });
                }
                mAdapter.notifyDataSetChanged();
                return false;
            }
        });

        popupMenu.show();
    }


    private void showDetails(final PhotoInfo info) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(SqliteTestActivity.this);

        View dialogView = getLayoutInflater().inflate(R.layout.item_sqlite_test_custom_dialog, null);

        ImageView imvAvata = (ImageView) dialogView.findViewById(R.id.imv_dialog_sqlite_avata);
        final EditText edtName = (EditText) dialogView.findViewById(R.id.edt_dialog_sqlite_name);
        final EditText edtDescription = (EditText) dialogView.findViewById(R.id.edt_dialog_sqlite_description);
        final EditText edtDownload = (EditText) dialogView.findViewById(R.id.edt_dialog_sqlite_download);
        final EditText edtView = (EditText) dialogView.findViewById(R.id.edt_dialog_sqlite_view);


        if (info == null) {
            Picasso.with(SqliteTestActivity.this)
                    .load(url)
                    .into(imvAvata);
            mBuilder.setTitle("Add");
            mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            mBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                   /* if (manager == null) {
                        manager = new SQLiteManager(SqliteTestActivity.this);
                    }*/
                    PhotoInfo photoInfo = new PhotoInfo();
                    /*photoInfo.setId(manager.getNextId());*/
                    photoInfo.setImages(url);
                    photoInfo.setName(edtName.getText().toString());
                    photoInfo.setDescription(edtDescription.getText().toString());
                    photoInfo.setDownload(Integer.parseInt(edtDownload.getText().toString()));
                    photoInfo.setView(Integer.parseInt(edtView.getText().toString()));

                    addPhoto(photoInfo);
                }
            });
        } else {
            if (!info.getImages().equals("")) {
                Picasso.with(SqliteTestActivity.this)
                        .load(info.getImages())
                        .into(imvAvata);
            }

            edtName.setText(info.getName());
            edtDescription.setText(info.getDescription());
            edtDownload.setText(String.valueOf(info.getDownload()));
            edtView.setText(String.valueOf(info.getView()));

            mBuilder.setTitle("Update");

            mBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    PhotoInfo photoInfo = new PhotoInfo();
                    photoInfo.setId(info.getId());
                    photoInfo.setImages(info.getImages());
                    photoInfo.setName(edtName.getText().toString());
                    photoInfo.setDescription(edtDescription.getText().toString());
                    photoInfo.setDownload(Integer.parseInt(edtDownload.getText().toString()));
                    photoInfo.setView(Integer.parseInt(edtView.getText().toString()));

                    updatePhoto(photoInfo);

                }
            });

            mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
        }

        mBuilder.setView(dialogView);
        mBuilder.create().show();
    }

    private void updatePhoto(PhotoInfo info) {
        /*if (manager == null) {
            manager = new SQLiteManager(SqliteTestActivity.this);
        }
        manager.updatePhoto(info);*/

        DatabaseUtility.getInstance(SqliteTestActivity.this).updatePhoto(info, info.getId());

        mPhoto.clear();
        loadPhoto();
    }

    private void addPhoto(PhotoInfo info) {
        if (DatabaseUtility.getInstance(SqliteTestActivity.this).insertPhoto(info)) {
            mPhoto.add(0, info);
            mAdapter.notifyDataSetChanged();
        }

    }
}
