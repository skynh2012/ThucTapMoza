package com.moza.thuctapmoza.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.config.WebserviceConfig;
import com.moza.thuctapmoza.model.GridTwo;
import com.moza.thuctapmoza.model.Photo;
import com.moza.thuctapmoza.model.PhotoInfo;
import com.moza.thuctapmoza.modelmanager.ModelManager;
import com.moza.thuctapmoza.modelmanager.ModelManagerListener;
import com.moza.thuctapmoza.modelmanager.ParserUtility;
import com.moza.thuctapmoza.network.ParameterFactory;
import com.moza.thuctapmoza.networkgson.ApiManager;
import com.moza.thuctapmoza.networkgson.ApiResponse;
import com.moza.thuctapmoza.util.ActionBarUtil;
import com.moza.thuctapmoza.view.adapter.GridTwoAdapter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NetworkActivity extends AppCompatActivity {
    RecyclerView rcvNetwork;
    GridTwoAdapter mAdapter;
    ArrayList<GridTwo> mArrTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBarUtil.setTitleCenterActionBar("Networking", getSupportActionBar());

        rcvNetwork = (RecyclerView) findViewById(R.id.rcv_network);
        mArrTwo = new ArrayList<>();
        mAdapter = new GridTwoAdapter(NetworkActivity.this, mArrTwo);

        rcvNetwork.setLayoutManager(new GridLayoutManager(NetworkActivity.this, 2, LinearLayoutManager.VERTICAL, false));
        rcvNetwork.setAdapter(mAdapter);

       /* ModelManager.getAllPhoto(NetworkActivity.this, 1, 1, 0, true, new ModelManagerListener() {
            @Override
            public void onError(VolleyError error) {
                Log.e("ERROR", error.getMessage());
            }

            @Override
            public void onSuccess(Object object) {
                Log.e("SUCCESS", object.toString());

                ArrayList<PhotoInfo> arrPhoto = ParserUtility.parsePhoto(object.toString());

                for (PhotoInfo info : arrPhoto) {
                    GridTwo two = new GridTwo();
                    two.setCover(info.getImages());
                    two.setName(info.getName());
                    two.setSex(false);
                    two.setAvatar(info.getImages());
                    two.setAge(info.getView());

                    mArrTwo.add(two);
                }
                mAdapter.notifyDataSetChanged();
            }
        });*/

       /* AndroidNetworking.get(WebserviceConfig.getAllPhoto(NetworkActivity.this))
                .addQueryParameter(ParameterFactory.createPhotoParams(1, 0, 2))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Succes", response.toString());
                        ArrayList<PhotoInfo> infos = ParserUtility.parsePhoto(response.toString());
                        for (PhotoInfo info : infos) {
                            GridTwo two = new GridTwo();
                            two.setCover(info.getImages());
                            two.setName(info.getName());
                            two.setSex(false);
                            two.setAvatar(info.getImages());
                            two.setAge(info.getView());

                            mArrTwo.add(two);
                        }
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("ERROR", anError.getMessage());
                    }
                });*/

        ApiManager.get(WebserviceConfig.getAllPhoto(NetworkActivity.this), ParameterFactory.setPhotoParams(1, 0, 2), new ApiManager.CompleteListener() {
            @Override
            public void onSuccess(ApiResponse response) {
                List<Photo> lstPhoto = response.getDataList(Photo.class);

                for (Photo info : lstPhoto) {
                    GridTwo two = new GridTwo();
                    two.setCover(info.getImages());
                    two.setName(info.getName());
                    two.setSex(false);
                    two.setAvatar(info.getImages());
                    two.setAge(info.getView());

                    mArrTwo.add(two);
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String message) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
