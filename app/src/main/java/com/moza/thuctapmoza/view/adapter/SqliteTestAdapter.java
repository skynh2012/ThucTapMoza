package com.moza.thuctapmoza.view.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.moza.thuctapmoza.R;
import com.moza.thuctapmoza.model.PhotoInfo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Administrator on 04/07/2017.
 */

public class SqliteTestAdapter extends ArrayAdapter {
    private ArrayList<PhotoInfo> mList;
    private LayoutInflater mInflater;
    private Context mContext;


    public SqliteTestAdapter(Context context, @LayoutRes int resource, ArrayList<PhotoInfo> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mList = objects;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_sqlite_test, parent, false);
            holder = new ViewHolder();
            holder.imvPhoto = (ImageView) convertView.findViewById(R.id.imv_photo_image);
            holder.tvId = (TextView) convertView.findViewById(R.id.tv_photo_id);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_photo_name);
            holder.tvDownload = (TextView) convertView.findViewById(R.id.tv_photo_download);
            holder.tvView = (TextView) convertView.findViewById(R.id.tv_photo_view);
            holder.chkChecked = (CheckBox) convertView.findViewById(R.id.chk_sqlite);
            holder.tvDescription = (TextView) convertView.findViewById(R.id.tv_photo_description);
            holder.chkChecked.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CheckBox chk = (CheckBox) view;
                    PhotoInfo info = (PhotoInfo) chk.getTag();
                    info.setCheck(chk.isChecked());
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        PhotoInfo info = mList.get(position);

        if (info != null) {
            if (!info.getImages().equals("")) {
                Picasso.with(mContext)
                        .load(info.getImages())
                        .into(holder.imvPhoto);
            }

            holder.tvId.setText("Id : " + info.getId());
            holder.tvName.setText("Name : " + info.getName());
            holder.tvDownload.setText("Download : " + info.getDownload());
            holder.tvView.setText("View : " + info.getView());
            holder.tvDescription.setText(info.getDescription());
            holder.chkChecked.setChecked(info.isCheck());
            holder.chkChecked.setTag(info);
        }

        return convertView;
    }


    public class ViewHolder {
        public ImageView imvPhoto;
        public TextView tvId, tvName, tvDownload, tvView, tvDescription;
        public CheckBox chkChecked;
    }


}
