package com.moza.thuctapmoza.app;

import android.app.Application;

import com.android.volley.toolbox.ImageLoader;
import com.androidnetworking.cache.LruBitmapCache;
import com.moza.thuctapmoza.network.ControllerRequest;
import com.moza.thuctapmoza.networkgson.BaseRequest;
import com.moza.thuctapmoza.networkgson.VolleyRequestManager;

/**
 * Created by Administrator on 03/07/2017.
 */

public class AppController extends ControllerRequest {
    private static AppController instance;
    private ImageLoader mImageLoader;
    public static int count = 0;

    public static AppController getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        //DataStoreManager.init(getApplicationContext());
        VolleyRequestManager.init(getApplicationContext());
        BaseRequest.init();
    }

   /* public ImageLoader getImageLoader() {
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(VolleyRequestManager.getRequestQueue(), new LruBitmapCache());
        }
        return this.mImageLoader;
    }*/
}
