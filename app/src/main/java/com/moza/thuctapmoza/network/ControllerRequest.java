package com.moza.thuctapmoza.network;


import java.util.Objects;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;

public class ControllerRequest extends Application {

    private RequestQueue requestQueue;
    public static final String TAG = ControllerRequest.class.getSimpleName();
    private static ControllerRequest controller;

    @Override
    public void onCreate() {
        super.onCreate();
        controller = this;
        AndroidNetworking.initialize(getApplicationContext());
    }

    /**
     * @return
     */

    public static ControllerRequest getInstance() {
        return controller;
    }

    /**
     * @return tráº£ vá»� má»™t Ä‘á»‘i tÆ°á»£ng cá»§a RequestQueue sá»­ dá»¥ng
     * Ä‘á»ƒ gá»­i request
     */
    private RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return requestQueue;
    }

    /**
     * @param request má»™t request báº¥t kÃ¬
     * @param tag     Ä‘Æ°á»£c sá»­ dá»¥ng setTag cho request
     * @param <T>     tham sá»‘ extends tá»« Object
     */
    public <T> void addToRequestQueue(Request<T> request, String tag) {
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(request);
    }

    /**
     * @param request
     * @param <T>     tham sá»‘ extends tá»« Object
     */
    public <T> void addToRequestQueue(Request<T> request) {
        request.setTag(TAG);
        getRequestQueue().add(request);

    }

    /**
     * @param tag
     */
    public void cancelRequest(Objects tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }
}