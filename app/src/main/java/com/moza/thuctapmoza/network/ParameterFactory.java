package com.moza.thuctapmoza.network;

import com.moza.thuctapmoza.model.User;

import java.util.HashMap;
import java.util.Map;

/**
 * ParameterFactory class builds parameters for web service apis
 */
public final class ParameterFactory {

    public static Map<String, String> createAlbumIdParams(String id) {
        Map<String, String> params = new HashMap<>();
        params.put("id", id + "");
        return params;
    }

    public static Map<String, String> createLogin(String email) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        return params;
    }

    public static Map<String, String> createAllCategory(String key) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("keyword", key);
        return params;
    }

    public static Map<String, String> createRegister(User user) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("full_name", user.getFullName());
        params.put("email", user.getEmail());
        return params;
    }

    public static Map<String, String> createPhotoParams(int id, int type, int page) {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("id",String.valueOf(id));
        parameters.put("type", String.valueOf(type));
        parameters.put("page", String.valueOf(page));
        return parameters;
    }

    public static HashMap<String, String> setPhotoParams(int id, int type, int page) {
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("id",String.valueOf(id));
        parameters.put("type", String.valueOf(type));
        parameters.put("page", String.valueOf(page));
        return parameters;
    }

    public static Map<String, String> createSearchParams(String key) {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("keyword", key);
        return parameters;
    }

    public static Map<String, String> createImage(String id) {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("id", id);
        return parameters;
    }
}
