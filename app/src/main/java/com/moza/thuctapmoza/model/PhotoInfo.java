package com.moza.thuctapmoza.model;

/**
 * Created by Administrator on 28/06/2017.
 */

public class PhotoInfo {
    private int id;
    private String name;
    private String description;
    private int download;
    private int view;
    private String images;
    private boolean check = false;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public PhotoInfo(int id, String name, String description, int download, int view, String images) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.download = download;
        this.view = view;
        this.images = images;
    }

    public PhotoInfo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDownload() {
        return download;
    }

    public void setDownload(int download) {
        this.download = download;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object obj) {
        return id == ((PhotoInfo) obj).getId();
    }
}
