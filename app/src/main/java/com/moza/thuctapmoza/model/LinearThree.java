package com.moza.thuctapmoza.model;

/**
 * Created by Administrator on 21/06/2017.
 */

public class LinearThree {
    private float rate;
    private String name;
    private String date;
    private String title;
    private String content;

    public LinearThree(float rate, String name, String date, String title, String content) {
        this.rate = rate;
        this.name = name;
        this.date = date;
        this.title = title;
        this.content = content;
    }

    public LinearThree() {
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
