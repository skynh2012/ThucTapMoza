package com.moza.thuctapmoza.model;

/**
 * Created by Administrator on 22/06/2017.
 */

public class GridOther {
    private String photo;
    private String topic;
    private int topicColor;
    private String time;
    private String title;
    private int type;
    public GridOther() {
    }

    public GridOther(String photo, String topic, int topicColor, String time, String title, int type) {
        this.photo = photo;
        this.topic = topic;
        this.topicColor = topicColor;
        this.time = time;
        this.title = title;
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getTopicColor() {
        return topicColor;
    }

    public void setTopicColor(int topicColor) {
        this.topicColor = topicColor;
    }
}
