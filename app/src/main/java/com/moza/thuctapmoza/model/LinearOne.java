package com.moza.thuctapmoza.model;

/**
 * Created by Administrator on 20/06/2017.
 */

public class LinearOne {
    private String avatar;
    private String name;
    private String time;
    private String message;

    public LinearOne(String avatar, String name, String time, String message) {
        this.avatar = avatar;
        this.name = name;
        this.time = time;
        this.message = message;
    }

    public LinearOne() {

    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
