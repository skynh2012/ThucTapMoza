package com.moza.thuctapmoza.model;

/**
 * Created by Administrator on 21/06/2017.
 */

public class GridTwo {
    private String cover;
    private String avatar;
    private String name;
    private int age;
    private boolean sex;

    public GridTwo() {
    }

    public GridTwo(String cover, String avatar, String name, int age, boolean sex) {
        this.cover = cover;
        this.avatar = avatar;
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }
}
