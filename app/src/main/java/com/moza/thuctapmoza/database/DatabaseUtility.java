package com.moza.thuctapmoza.database;

import android.content.Context;

import com.moza.thuctapmoza.config.DatabaseConfig;
import com.moza.thuctapmoza.database.binder.ImageBinder;
import com.moza.thuctapmoza.database.mapper.PhotoMapper;
import com.moza.thuctapmoza.model.PhotoInfo;

import java.util.List;

public final class DatabaseUtility {
    private static PrepareStatement statement;
    private static DatabaseUtility instance = null;

    /**
     * Constructor
     */
    public DatabaseUtility(Context context) {
        statement = new PrepareStatement(context);
    }

    /**
     * get class instance
     *
     * @return
     */
    public static DatabaseUtility getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseUtility(context);
        }
        return instance;
    }

    private static String STRING_SQL_INSERT_INTO_TABLE_TEST = "INSERT OR REPLACE INTO "
            + DatabaseConfig.TABLE_NAME
            + "("
            + DatabaseConfig.COLUMN_ID
            + ","
            + DatabaseConfig.COLUMN_NAME
            + ","
            + DatabaseConfig.COLUMN_IMAGE
            + " ,"
            + DatabaseConfig.COLUMN_DESCRIPTION
            + " ,"
            + DatabaseConfig.COLUMN_DOWNLOAD
            + " ,"
            + DatabaseConfig.COLUMN_VIEW + ") VALUES (?, ?, ?,?,?,?)";


    private static String STRING_SQL_UPDATE_TEST(PhotoInfo info) {
        return DatabaseConfig.COLUMN_NAME
                + "='" + info.getName() + "'"
                + ","
                + DatabaseConfig.COLUMN_IMAGE
                + "='" + info.getImages() + "'"
                + ","
                + DatabaseConfig.COLUMN_DESCRIPTION
                + "='" + info.getDescription() + "'"
                + ","
                + DatabaseConfig.COLUMN_DOWNLOAD
                + "='" + info.getDownload() + "'"
                + ","
                + DatabaseConfig.COLUMN_VIEW
                + "='" + info.getView() + "'";
    }

    public static boolean checkInfo(String Id) {
        List<PhotoInfo> list = statement.select(DatabaseConfig.TABLE_NAME, "*",
                DatabaseConfig.COLUMN_ID + " = '" + Id + "'",
                new PhotoMapper());
        if (list.size() > 0) {
            return true;
        }
        return false;
    }

    public boolean insertPhoto(PhotoInfo info) {
        if (info.getId() <= 0) {
            info.setId(getNextId());
        }
        return statement.insert(STRING_SQL_INSERT_INTO_TABLE_TEST, info, new ImageBinder());
    }

    public static boolean updatePhoto(PhotoInfo info, int id) {
        return statement.update(DatabaseConfig.TABLE_NAME, STRING_SQL_UPDATE_TEST(info),
                DatabaseConfig.COLUMN_ID + "='" + id + "'");
    }

    public static boolean deletePhoto(PhotoInfo info) {
        return statement.query(
                "DELETE FROM " + DatabaseConfig.TABLE_NAME + " where "
                        + DatabaseConfig.COLUMN_ID + "='" + info.getId()
                        + "'", null);
    }

    public List<PhotoInfo> getListPhoto() {
        return statement.select(DatabaseConfig.TABLE_NAME, "*", "",
                new PhotoMapper());
    }

    public static List<PhotoInfo> getDetailPhoto(String id) {
        return statement.select(DatabaseConfig.TABLE_NAME, "*", DatabaseConfig.COLUMN_ID + "='" + id + "'",
                new PhotoMapper());
    }

    private int getNextId() {
        int nextId = 0;
        List<PhotoInfo> lstPhoto = getListPhoto();

        for (PhotoInfo info : lstPhoto) {
            if (info.getId() > nextId) {
                nextId = info.getId();
            }
        }
        return nextId + 1;
    }
}