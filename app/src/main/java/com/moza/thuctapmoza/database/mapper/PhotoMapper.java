package com.moza.thuctapmoza.database.mapper;

import android.database.Cursor;

import com.moza.thuctapmoza.config.DatabaseConfig;
import com.moza.thuctapmoza.database.CursorParseUtility;
import com.moza.thuctapmoza.database.IRowMapper;
import com.moza.thuctapmoza.model.PhotoInfo;

public class PhotoMapper implements IRowMapper<PhotoInfo> {
    @Override
    public PhotoInfo mapRow(Cursor row, int rowNum) {
        PhotoInfo info = new PhotoInfo();
        info.setId(CursorParseUtility.getInt(row, DatabaseConfig.COLUMN_ID));
        info.setName(CursorParseUtility.getString(row, DatabaseConfig.COLUMN_NAME));
        info.setImages(CursorParseUtility.getString(row, DatabaseConfig.COLUMN_IMAGE));
        info.setDescription(CursorParseUtility.getString(row, DatabaseConfig.COLUMN_DESCRIPTION));
        info.setDownload(CursorParseUtility.getInt(row, DatabaseConfig.COLUMN_DOWNLOAD));
        info.setView(CursorParseUtility.getInt(row, DatabaseConfig.COLUMN_VIEW));
        return info;
    }
}