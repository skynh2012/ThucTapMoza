package com.moza.thuctapmoza.networkgson;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.moza.thuctapmoza.R;

/**
 * Created by Administrator on 03/07/2017.
 */

public class MyProgressDialog extends Dialog {
    public MyProgressDialog(Context context) {
        super(context);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(
                R.drawable.ic_like_page);
        setContentView(R.layout.layout_progress_dialog);

    }
}
