package com.moza.thuctapmoza.networkgson;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.moza.thuctapmoza.app.AppController;

/**
 * Created by Administrator on 03/07/2017.
 */

public class NetworkUtility {

    public static boolean isNetworkAvailable() {
        ConnectivityManager conMgr = (ConnectivityManager) AppController.getInstance()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null) {
            return false;
        }
        if (!i.isConnected()) {
            return false;
        }
        if (!i.isAvailable()) {
            return false;
        }
        return true;
    }
}
