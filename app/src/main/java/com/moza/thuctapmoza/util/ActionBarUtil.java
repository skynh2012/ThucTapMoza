package com.moza.thuctapmoza.util;

import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.TextView;

import com.moza.thuctapmoza.R;

/**
 * Created by Administrator on 20/06/2017.
 */

public class ActionBarUtil {
    public static void setTitleCenterActionBar(String title, ActionBar actionBar) {
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.layout_actionbar_custom_title);
        actionBar.setDisplayHomeAsUpEnabled(true);
        View view = actionBar.getCustomView();
        TextView titleView = (TextView) view.findViewById(R.id.tv_custom_actionbar);
        titleView.setText(title);
    }
}
